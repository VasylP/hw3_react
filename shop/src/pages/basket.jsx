import CardList from "../components/CardList";


export function Basket (props){
    const {basket, fav, addToFav, delFromBasket, products} = props;
    if (basket.length > 0) {
        let arr = [];
        products.forEach(element => {
            if(basket.includes(element.barcode)){
                arr.push(element);
            }
        });
        return (
            <>
            <CardList
                    arr={arr}
                    products={products}
                    fav={fav}
                    addToFav={addToFav}
                    delFromBasket={delFromBasket}
                    delBtn={true}
                />
            </>
        )
    }
    return <h1 className="no_data">No products in your basket</h1>
}