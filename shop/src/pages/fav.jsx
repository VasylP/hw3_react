import CardList from "../components/CardList";

export function Fav (props){
    const {fav, addToFav, addToBasket, products} = props;
    if (fav.length > 0){
        let arr = [];
        products.forEach(element => {
            if(fav.includes(element.barcode)){
                arr.push(element);
            }
        });
        return (
            <>
                <CardList
                    arr={arr}
                    products={products}
                    fav = {fav}
                    addToFav = {addToFav}
                    addToBasket = {addToBasket}
                    buyBtn = {true}
                />
            </>
        )
    };
        return <h1 className="no_data">You dont have a favourites products</h1>
}