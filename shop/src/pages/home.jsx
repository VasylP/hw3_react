import CardList from "../components/CardList";

export function Home (props) {
    const {products, fav, addToFav, addToBasket} = props;
    return (
        <CardList
        arr={products}
        products={products}
        fav={fav}
        addToFav={addToFav}
        addToBasket={addToBasket}
        buyBtn={true}
        />
    )
}