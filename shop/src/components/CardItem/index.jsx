import { useState } from "react";
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar as favIcon } from "@fortawesome/free-solid-svg-icons";
import { faStar as noFav } from '@fortawesome/free-regular-svg-icons';
import { faCircleXmark } from "@fortawesome/free-solid-svg-icons";
import Button from '../Button';
import Modal from '../Modal';
import "./cardItem.scss"

export default function CardItem ( props ){
    const [isModalActive, setIsModalActive] = useState(false);
    const {fav, picture, prodName, prodPrice, code, prodColor, cheangeFav, addToBasket, delFromBasket, buyBtn, delBtn} = props;
    const toggleModal = () => {
        setIsModalActive(!isModalActive);
    }
    return (
        <>
        <div className="card__wrapper">
        {delBtn ? 
                <div className="del__icon">
                    <FontAwesomeIcon icon={faCircleXmark} onClick={toggleModal}/>
                </div> : null}
        <p className="card-description__prod-name">{prodName}</p>
            <div className="info-wrapper">
                <div className="card__img-wrapper">
                    <img src={picture} alt={`${prodName} product`}/>
                </div>
                <div className="card__description">
                    <p className="card-description__prod-price">price: <span>{prodPrice}$</span></p>
                    <p className="card-description__prod-barcode">code: {code}</p>
                    <p className="card-description__prod-color">color: <span>{prodColor}</span></p>
                </div>
            </div>
            <div className="card__btn-wrapper">
                    {!fav.includes(code) ? 
                        <FontAwesomeIcon icon={noFav} className="fav-icon" onClick={cheangeFav} data-id={code}/> :
                        <FontAwesomeIcon icon={favIcon} className="fav-icon" onClick={cheangeFav} data-id={code}/>
                    }
                    {buyBtn ?
                    <Button
                    text={"Buy"}
                    onClick={toggleModal}
                    /> : null}
            </div>
        </div>

        <Modal
        isActive={isModalActive}
        tittle={buyBtn ?'Do you want add this product to basket?' : 'Do you want delete this product from basket?'}
        toggleModal={toggleModal}
        okButton={buyBtn ? addToBasket : delFromBasket}
        picture={picture}
        name={prodName}
        color={prodColor}
        price={prodPrice}
        code={code}
        okButtonCode={code}
        />
        </>
    )
}

CardItem.propTypes = {
    picture: PropTypes.string,
    prodName: PropTypes.string,
    prodPrice: PropTypes.string,
    code: PropTypes.string,
    prodColor: PropTypes.string,
    cheangeFav: PropTypes.func,
    addToBasket: PropTypes.func,
}
CardItem.defaultProps = {
    picture: "https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/No-Image-Placeholder.svg/330px-No-Image-Placeholder.svg.png",
    prodName: "Product name",
    prodPrice: "product price",
    code: "product barcode",
    prodColor: "product color",
    cheangeFav: ()=>{
        console.log("is Fav cheange");
    },
    addToBasket: ()=>{
        console.log("add to Basket");
    },
}