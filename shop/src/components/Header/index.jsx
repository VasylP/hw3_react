import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import { faHouse } from '@fortawesome/free-solid-svg-icons';
import { faBasketShopping } from '@fortawesome/free-solid-svg-icons';
import "./header.scss";
import { NavLink, Outlet } from "react-router-dom";
// import { useGetFav, useGetBasket } from "../../hooks";

export default function Header ({fav, basket}) {
    // const fav = useGetFav();
    // const basket = useGetBasket();
    return (
        <>
        <header className='header'>
            <NavLink to="/">
                <FontAwesomeIcon icon={faHouse} />
            </NavLink>
            <div className='header__fav-wrapper'>
                <p className='fav__length'>{fav.length}</p>
                <NavLink to="/fav">
                    <FontAwesomeIcon icon={faStar} />
                </NavLink>
            </div>
            <div className='header__basket-wrapper'>
                <p className='basket__length'>{basket.length}</p>
                <NavLink to="/basket">
                    <FontAwesomeIcon icon={faBasketShopping} />
                </NavLink>
            </div>
        </header>
        <Outlet/>
        </>
    )
}

// Header.propTypes = {
//     fav: PropTypes.array,
//     basket: PropTypes.array,   
// }
// Header.defaultProps = {
//     fav: [],
//     basket: [],
// }