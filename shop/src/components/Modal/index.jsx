import PropTypes from "prop-types";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark } from '@fortawesome/free-solid-svg-icons';
import "./modal.scss";
import Button from "../Button";

export default function Modal (props){
  const {isActive, 
    tittle,
    closeButton, 
    // onClick, 
    toggleModal, 
    okButton,
    picture,
    name,
    color,
    price,
    code,
    okButtonCode,
  } = props;
    if (!isActive){
        return null;
    }
    return (
      <>
            <div className="modal-overlay"
                onClick={toggleModal}
            >
                <div className="modal-window">
                    <div className="modal-header">
                        <h3>
                            {tittle}
                        </h3>
                        <div className="close-btn">
                            {closeButton === true ? <FontAwesomeIcon 
                            icon={faXmark} 
                            onClick={toggleModal} 
                            /> : '' }
                        </div>
                    </div>
                    <div className="modal-text">
                        <img className="product-img" src={picture}/>
                        <div className="modal-text__product">
                        <p className="product-name">modal: {name}</p>
                        <p className="product-price">price: {price}</p>
                        <p className="product-color">color: {color}</p>
                        <p className="product-code">code: {code}</p>
                        </div>
                    </div>
                    {/* <p className="modal-text__text">{text}</p> */}
                    <div className="modal-btn">
                        <Button
                        text= "Ok"
                        onClick={okButton}
                        code={okButtonCode}
                        />
                        <Button
                        text= "Cancel"
                        onClick={toggleModal}
                        />
                    </div>
                </div>
            </div>
            </>

    // <div className="modal_overlay" onClick={toggleModal}>
    //   <div className="modal_window">
    //     <div className="modal-header">{tittle}</div>
    //     <div className="modal-text">
    //       <div className="product-info__img-wrapper">
    //         <img src={picture} alt={`${name} product` className="product-img"}/>
    //       </div>
    //       <div className="product-info__info-wrapper">
    //         <p>{name}</p>
    //         <p>{price}</p>
    //         <p>{color}</p>
    //         <p>{code}</p>
    //       </div>
    //     </div>
    //     <div className="modal_window_buttons">
    //       <Button
    //         text={"Ok"}
    //         onClick={() => {
    //           toggleModal();
    //           okButton({ picture, name, color, price, code });
    //         }}
    //         code={okButtonCode}
    //       />
    //       <Button
    //         text={"Сancel"}
    //         onClick={() => {
    //           toggleModal();
    //         }}
    //       />
    //     </div>
    //   </div>
    // </div>
    )
}

Modal.propTypes = {
    isActive: PropTypes.bool,
    tittle: PropTypes.string,
    // onClick: PropTypes.func,
    toggleModal: PropTypes.func,
    okButton: PropTypes.func,
}

Modal.defaultProps = {
    isActive: false,
    tittle: "Modal ask",
    // onClick: ()=> {
    //     console.log("click overlay");
    // },
    toggleModal: ()=> {
        console.log("modal toggle");
    },
    okButton: ()=> {
        console.log("ok button clicked");
    },
}