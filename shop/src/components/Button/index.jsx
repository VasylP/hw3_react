import PropTypes from "prop-types";
import "./button.scss"

export default function Button(props){
    return <button className="btn" onClick={props.onClick} data-id={props.code}>{props.text}</button>
}
Button.prototype = {
    text: PropTypes.string,
    onClick: PropTypes.func,
    code: PropTypes.string,
}
Button.defaultProps = {
    text: "Click me!",
    onClick: ()=>{
        console.log("Clicked");
    },
    code: "product code",
}