import PropTypes from 'prop-types';
import './cardList.scss';
import CardItem from "../CardItem";


export default function CardList (props){
    const {arr, products, fav, addToFav, addToBasket, delFromBasket, buyBtn, delBtn} = props;
    if (products === null) {
        return <h1>Loading...</h1>;
      }
    return (
        <>
        <ul className="card-list">
            {arr.map(({ name, price, picture, barcode, color }) => (
                <li key={barcode} className="card-list__item">
                    <CardItem
                    fav={fav}
                    picture={picture}
                    prodName={name}
                    prodPrice={price}
                    code={barcode}
                    prodColor={color}
                    cheangeFav={addToFav}
                    addToBasket={addToBasket}
                    delFromBasket={delFromBasket}
                    buyBtn={buyBtn}
                    delBtn={delBtn}
                    />
                </li>
            ))}
        </ul>
        </>
    )

}

CardList.propTypes = {
    products: PropTypes.array,
    fav: PropTypes.array,
    addToFav: PropTypes.func,
    addToBasket: PropTypes.func,
    delBtn: PropTypes.bool,
    buyBtn: PropTypes.bool,
}
CardList.defaultTypes = {
    products: [],
    fav: [],
    addToFav: () => {console.log("add to Fav");},
    addToBasket: () => {console.log("add to Basket");},
    delBtn: false,
    buyBtn: false,
}
