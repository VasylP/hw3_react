import { Routes, Route } from "react-router-dom";
import { Home, Fav, Basket } from './pages';
import Header from './components/Header';
import { useEffect, useState } from "react";

function App() {
  // const products = useGetProducts();
  // let fav = useGetFav();
  // let basket = useGetBasket();

  const [products, setProducts] = useState(null);
  const [fav, setFav] = useState([]);
  const [basket, setBasket] = useState([]);
  let data = localStorage;
  function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

  useEffect(() => {
    fetch('./goods.json')
            .then(response => response.json())
            .then(data => setProducts(data));
    if (data.getItem("favourites") && !IsJsonString(data.getItem("favourites"))) {
      data.removeItem("favourites");
    }
    if (data.getItem("basket") && !IsJsonString(data.getItem("basket"))) {
      data.removeItem("basket");
    }
    // if (data.length > 0){
      if (data.getItem("favourites") && IsJsonString(data.getItem("favourites"))){
        setFav(JSON.parse(data.getItem("favourites")));
      }
      if (data.getItem("basket") && IsJsonString(data.getItem("basket"))) {
        setBasket(JSON.parse(data.getItem("basket")));
      }
    // }
  }, [data]);


  const addToFav = (e) => {
    const target = e.target.getAttribute('data-id');
    let allFav = [...fav];
    if (allFav.includes(target)){
      let newAllFaw = [];
      allFav.forEach(el => {
        if (el !== target) {
          newAllFaw.push(el)
        }
      })
      localStorage.setItem("favourites", JSON.stringify(newAllFaw));
      // return fav = newAllFaw;
      return setFav(newAllFaw);
    }
    allFav.push(target);
    localStorage.setItem("favourites", JSON.stringify(allFav));
    // return fav = allFav;
    return setFav(allFav);
  }
  const addToBasket = (e) => {
    const target = e.target.getAttribute('data-id');
    let allBasket = [...basket];
    if (allBasket.includes(target)){
      return alert("This product is in your basket!")
    }
    allBasket.push(target);
    localStorage.setItem("basket", JSON.stringify(allBasket));
    // return basket = allBasket;
    return setBasket(allBasket);
  }
  const delFromBasket = (e) => {
    const target = e.target.getAttribute('data-id');
    let allBasket = [...basket];
    if (allBasket.includes(target)){
      let newAllBasket = [];
      allBasket.forEach(el => {
        if (el !== target) {
          newAllBasket.push(el)
        }
      })
      localStorage.setItem("basket", JSON.stringify(newAllBasket));
      // return basket = newAllBasket;
      return setBasket(newAllBasket);
    }
  }

if (!products) {
  return <h1>Loading...</h1>
}


  return (
    <>
    <Header
    fav={fav}
    basket={basket}
    />
    <Routes>
      <Route 
        path='/' 
        element={<Home
        products={products}
        fav={fav}
        addToFav={addToFav}
        addToBasket={addToBasket}
        />}
      />
      <Route 
        path='/fav' 
        element={<Fav
          products={products}
          fav={fav}
          addToFav={addToFav}
          addToBasket={addToBasket}
        />}
      />
      <Route 
        path='/basket' 
        element={<Basket
          products={products}
          basket={basket}
          fav={fav}
          addToFav={addToFav}
          delFromBasket={delFromBasket}
        />}
      />
    </Routes>
    </>
  );
}

export default App;
